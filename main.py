import pyodbc 
import os
from tqdm import tqdm
import numpy as np
from bs4 import BeautifulSoup


server = '127.0.0.1,1433' 
database = 'master'
username = 'sa' 
password = 'YourStrongkjf338rw' 

# cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password,autocommit=True)
cursor = conn.cursor()



# ------------------------------- init

# drop table if exists

def drop():
    rq='''
    DROP TABLE IF EXISTS Classification;
    DROP TABLE IF EXISTS Detection;
    DROP TABLE IF EXISTS Segmentation;
    DROP TABLE IF EXISTS Label;
    DROP TABLE IF EXISTS Image;
    '''
    cursor.execute(rq)


drop()


def affichage(rq):
    cursor.execute(rq)
    # try: 
    for i in cursor:
        print(i)
    # except pyodbc.ProgrammingError:
        # print('table vide')
    print('\n\n')






# ------------------------------- create table

# image
rq='''
CREATE TABLE Image (
    id VARCHAR(100) NOT NULL,
    source VARCHAR(30)
    primary key (id)
 );
'''
# source=dataset

cursor.execute(rq)
# label
rq='''
CREATE TABLE Label (
    id VARCHAR(30),
    im VARCHAR(100),
    FOREIGN KEY (im) REFERENCES Image(id),
    primary key (id),
 );
'''

#  primary key (id,im),
cursor.execute(rq)

rq='''
insert into Image values ('varcharrrr', 'sourcepath')
'''

# cursor.execute(rq)

rq='''select * from Image'''
# a=cursor.execute(rq)
# print(a)
affichage(rq)
# exit(0)





# table classification forcement si si dectection et segmentation

rq='''
CREATE TABLE Classification (
    id INTEGER NOT NULL,
    label VARCHAR(30),
    md VARCHAR(30),
    FOREIGN KEY (label) REFERENCES Label(id),
    primary key (id)
 );
'''
cursor.execute(rq)









def get_md(li):
    '''
    resulset
    ---
    str label
    list position du label
    '''
    
    
    label=li.contents[1].string
    
    
    xmin=li.bndbox.xmin.string
    ymin=li.bndbox.ymin.string
    xmax=li.bndbox.xmax.string
    ymax=li.bndbox.ymax.string

    # coord=li.contents[-2]
    # xmin=coord.xmin.string
    # ymin=coord.ymin.string
    # xmax=coord.xmax.string
    # ymax=coord.ymax.string
    return label,[xmin,ymin,xmax,ymax]






wk=os.getcwd()
base='/dataset/VOCdevkit/VOC2012/Annotations/'
dataset="VOC2012"
ims = np.array(os.listdir(wk+base)[:5])


# ajout par bulk dans tables Images

n_bulk=3
base_rq='''
INSERT INTO Image (id, source)
VALUES
'''
ss=''
id_cls=[]
for k,im in enumerate(ims):
    ss+='''('''+"'"+im[:-4]+"'"+''','''+"'"+dataset+"'"+'''),'''
    id_cls.append(im[:-4])
    # print(k,k%n_bulk)

    # push dans table image
    if (k%n_bulk)==0:
        rq=base_rq+ss[:-1]+''';'''
        cursor.execute(rq)
        ss=''

        # on ajoute les md
        for idd in id_cls:
            print()
            with open(wk+base+idd+'.xml', 'r') as f:
                data = f.read()

            ses_labels=[]
            bs_data = BeautifulSoup(data, 'xml') 
            objs=bs_data.find_all('object')
            for obj in objs:
                a,b=get_md(obj)
                ses_labels.append(a)
                # print(a,b,idd)

            # suppression de doublon pour la table label
            print(ses_labels)
            ses_labels = list(dict.fromkeys(ses_labels))
            print(ses_labels)

            # push md classification







    


# reliquat










exit(0)

# md a b c d

rq='''
CREATE TABLE Detection (
    id INTEGER NOT NULL,
    label VACHAR(30),
    md VARCHAR(50),
    FOREIGN KEY (label) REFERENCES Label(id),
    primary key (id)
 );
'''

# cursor.execute(rq)
rq=''' 
CREATE TABLE Segmentation (
    id INTEGER NOT NULL,
    label VACHAR(30),
    md VARCHAR(500),
    FOREIGN KEY (label) REFERENCES Label(id),
    primary key (id)
 );
'''

# cursor.execute(rq)





# 17126
